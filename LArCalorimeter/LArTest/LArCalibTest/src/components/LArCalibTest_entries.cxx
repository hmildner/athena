#include "LArCalibTest/LArCablingChecker.h"
#include "LArCalibTest/LArFakeCellFactor.h"
#include "LArCalibTest/LArReadHadDMCoeffFile.h"
#include "LArCalibTest/LArReadHadDMCoeffFile2.h"
#include "LArCalibTest/DumpCaloBadChannels.h"
#include "LArCalibTest/LArHVTest.h"



DECLARE_COMPONENT( LArCablingChecker )
DECLARE_COMPONENT( LArReadHadDMCoeffFile )
DECLARE_COMPONENT( LArReadHadDMCoeffFile2 )
DECLARE_COMPONENT( LArFakeCellFactor )
DECLARE_COMPONENT( DumpCaloBadChannels )
DECLARE_COMPONENT( LArHVTest )

